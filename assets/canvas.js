let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let mouseIsDown = false;
let radius = 10;

canvas.width = 900;
canvas.height = 600;
ctx.fillStyle = 'black';
ctx.lineWidth = radius*2;

canvas.onmousedown = function(e) {
  console.log('Down');
  mouseIsDown = true;
}

canvas.onmouseup = function(e) {
  console.log('Up');
  mouseIsDown = false;
  ctx.beginPath();
}

canvas.onmousemove = function(e) {
  console.log('Move');
  if(mouseIsDown)
  {
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(e.offsetX, e.offsetY, radius, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(e.offsetX, e.offsetY);
  }
}

canvas.onclick = function(e) {
  console.log('Clicked');
  ctx.beginPath();
  ctx.arc(e.offsetX, e.offsetY, radius, 0, Math.PI * 2, true);
  ctx.fill();
}

// TOOLS
//Clear all
let clearAll = document.getElementById('clearAllBtn');
clearAll.onclick = function(e) {
  cxt.fillStyle = "white";
  ctx.fillRect(0,0,canvas.width,canvas.height);
}
