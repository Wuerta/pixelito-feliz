require('../styles/main.css');
'use strict';
import React from 'react';
import ReactDOM from "react-dom";
ReactDOM.render(
  <div className="myDiv">Hello Electron!</div>,
  document.getElementById("root")
);
